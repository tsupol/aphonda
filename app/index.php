<?php
$asset_path = "./";
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <!--  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>-->
  <!--  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />-->
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
  <meta name="description" content="AP Honda - Race to the Dream">
  <title>AP Honda | Race to the Dream</title>
  <link rel="canonical" href="http://aphonda.artplore.com"/>
  <link rel="stylesheet" href="fonts/ap-honda/stylesheet.css" type="text/css" media="all"/>
  <link rel="stylesheet" type="text/css" media="all"
        href="<?php echo $asset_path ?>css/main.css?<?php echo time(); ?>"/>
</head>
<body>

<div id="main-nav">
</div>

<div id="sec-main" class="layout-outer">
  <div class="layout-inner">

    <!-- main title-->
    <div id="main-title">
      <img src="./imgs/main-title.png"/>
      <h2 class="main-subtitle">
        สปิริตไทย... <span class="color1">ท้าทายสู่ฝัน</span>
      </h2>
    </div>

    <!-- main title-->
    <div id="main-quote">
      <h3>เพราะเราเชื่อว่า</h3>
      <h4>
        "ไม่มีอะไรยิ่งใหญ่เกินไป<br class="show-sm"/>
        สำหรับฝันของคนไทย"
      </h4>
      <p>ขอเพียงมีพลังมุ่งมั่นตั้งใจ...ทุ่มเทฝึกฝนพัฒนา...<br class="show-sm"/>มีสปิริตไม่หยุดพยายาม<br class="hide-sm"/>
        ร่วมท้าทายทุกขีดจำกัดไปด้วยกัน<br class="show-sm"/>เพื่อประกาศศักดาสปิริตคนไทย</p>
    </div>

    <div id="scroll-wrap" class="flex-col-center hide-sm">
      <img id="mouse" src="./imgs/mouse.png"/>
      <h2>Please Scroll Down</h2>
      <div class="v-line"></div>
    </div>

    <div id="scroll-wrap-m" class="flex-col-center show-sm">
      <img id="arrow" src="./imgs/arrow-down.png"/>
      <h2>Slide Down</h2>
    </div>


  </div>
</div>
<div id="sec-excerpt">
  <div class="_content flex-col-center">
    <img class="_logo" src="./imgs/logo-road-to-mg.png"/>
    <h2>สู่ความฝันที่ยิ่งใหญ่<br/>
      สร้างความภาคภูมิใจให้คนไทยทั้งชาติ</h2>
    <h3>เส้นทางนักแข่งไทย<br class="show-sm">สู่สนาม
      <span class="color1">MotoGP<sup>TM</sup></span>
    </h3>
    <p>
      <span class="__smaller">ในปี <b>2025</b> โดยมี <b>Road Map</b> ที่ชัดเจน<br/>
      จากโครงสร้างที่ได้มาตรฐานทุกขั้นตอนในทุกระดับ</span>
    </p>
  </div>

  <!-- Cloud -->
  <img id="cloud" src="./imgs/cloud.png"/>

</div>
<div id="sec-action">

  <!-- Bike -->
  <img id="bike" src="./imgs/motorcycle.png"/>

  <div id="timeline-wrap">
    <div id="timeline">
      <div class="chevron __left"></div>
      <div class="_inner">
        <div class="_inner_inner">
          <div class="_line"></div>

          <!-- Event Dots 1 -->
          <div class="event active">
            <div class="_label">
              <img src="./imgs/events/event-tlogo-1.png"/>
            </div>
            <div class="dot"></div>
            <div class="connector"></div>
            <div class="event-desc __ed-1">
              <div class="_banner"></div>
              <div class="_desc">
                จุดเริ่มต้นเส้นทางสู่มอเตอร์สปอร์ตระดับโลก<br/>
                <b>นักแข่งอายุ 9-14 ปี</b><br/>
                รถที่ใช้ในการฝึกและแข่งขันทักษะ<br/>
                <b>Honda NSF100</b> (นำเข้าจากประเทศญี่ปุ่นทั้งคัน)<br/>
                สนามแข่งขัน : 7 สนาม (14 Races)
              </div>
            </div>
          </div>
          <!-- Event Dots 2 -->
          <div class="event">
            <div class="_label">
              <img src="./imgs/events/event-tlogo-2.png"/>
            </div>
            <div class="dot"></div>
            <div class="connector"></div>
            <div class="event-desc __ed-2">
              <div class="_banner"></div>
              <div class="_desc">
                ครั้งแรกกับความท้าทายระดับประเทศ<br/>
                <b>นักแข่งอายุ 14-20 ปี</b><br/>
                รถที่ใช้ในการแข่งขัน <b>Honda NSF250R</b><br/>
                สนามแข่งขัน : 6 สนาม (12 Races)
              </div>
            </div>
          </div>
          <!-- Event Dots 3 -->
          <div class="event">
            <div class="_label">
              <img src="./imgs/events/event-tlogo-3.png"/>
            </div>
            <div class="dot"></div>
            <div class="connector"></div>
            <div class="event-desc __ed-3">
              <div class="_banner"></div>
              <div class="_desc">
                ก้าวแรกสู่การแข่งขันระดับเอเชีย นักแข่งอายุไม่เกิน 19 ปี<br/>
                รถที่ใช้ในการแข่งขัน <b>Honda NSF250R</b><br/>
                สนามแข่งขัน : 6 สนาม (12 Races)<br/><br/>
                นักแข่งไทยในปัจจุบัน<br/>
                <b>ก๊อง-ธัชกร บัวศรี</b> แชมป์ไทยแลนด์ ทาเลนท์คัพ 2017<br/>
                และ <b>ไอซ์-นิติพงษ์ แสงสว่าง</b> นักบิดดาวรุ่งที่มีอายุเพียง 15 ปี
              </div>
            </div>
          </div>
          <!-- Event Dots 4 -->
          <div class="event">
            <div class="_label">
              <img src="./imgs/events/event-tlogo-4.png"/>
            </div>
            <div class="dot"></div>
            <div class="connector"></div>
            <div class="event-desc __ed-4">
              <div class="_banner"></div>
              <div class="_desc">
                ท้าทายสู่รายการชิงแชมป์แห่งทวีปยุโรป<br/>
                รถที่ใช้ในการแข่งขัน <b>Honda NSF250RW</b><br/>
                สนามแข่งขัน : 8 สนาม (12 Races)<br/><br/>
                นักแข่งไทยในปัจจุบัน<br/>
                <b>ก้อง-สมเกียรติ จันทรา</b> เจ้าของแชมป์เอเชีย ทาเลนท์คัพ 2016<br/>
                สังกัดทีม A.P. Honda Racing Thailand หมายเลข 35
              </div>
            </div>
          </div>
          <!-- Event Dots 5 -->
          <div class="event">
            <div class="_label">
              <img src="./imgs/events/event-tlogo-5.png"/>
            </div>
            <div class="dot"></div>
            <div class="connector"></div>
            <div class="event-desc __ed-5">
              <div class="_banner"></div>
              <div class="_desc">
                จุดเริ่มต้นความท้าทายระดับโลก<br/>
                รถที่ใช้ในการแข่งขัน 250cc <b>Honda NSF250RW</b><br/>
                สนามแข่งขัน : 19 สนาม (19 Races)<br/><br/>
                นักแข่งหนึ่งเดียวของไทย <b>ชิพ-นครินทร์ อธิรัฐภูวภัทร์</b><br/>
                นักแข่งไทยแบบเต็มฤดูกาลเป็นปีที่ 2<br/>
                สังกัดทีม Honda Team Asia หมายเลข 41
              </div>
            </div>
          </div>
          <!-- Event Dots 6 -->
          <div class="event">
            <div class="_label">
              <img src="./imgs/events/event-tlogo-6.png"/>
            </div>
            <div class="dot"></div>
            <div class="connector"></div>
            <div class="event-desc __ed-6">
              <div class="_banner"></div>
              <div class="_desc">
                การแข่งขันระดับโลกที่เพิ่มดีกรีความท้าทาย<br/>
                ด้วยขนาดเครื่องยนต์ที่ใหญ่ขึ้น<br/>
                รถที่ใช้ในการแข่งขัน 600cc 4 สูบ<br/>
                สนามแข่งขัน : 19 สนาม (19 Races)<br/><br/>
                นักแข่งโมโตทู คนแรกของประเทศไทย <b>ฟิล์ม-รัฐภาคย์ วิไลโรจน์</b><br/>
                <b>ช่วงปี 2007-2013</b>
              </div>
            </div>
          </div>
          <!-- Event Dots 7 -->
          <div class="event">
            <div class="_label">
              <img src="./imgs/events/event-tlogo-7.png"/>
            </div>
            <div class="dot"></div>
            <div class="connector"></div>
            <div class="event-desc __ed-7">
              <div class="_banner"></div>
              <div class="_desc">
                การแข่งขันรุ่น Top Class กับเทคโนโลยีระดับโลก<br/>
                รถที่ใช้ในการแข่งขัน 1000cc<br/>
                รุ่นรถในปัจจุบันของฮอนด้า <b>Honda RC213V</b><br/>
                สนามแข่งขัน : 19 สนาม (19 Races)
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="chevron __right"></div>
    </div>
  </div>

  <!-- Event Containers for Mobile -->
  <div id="mobile-events"></div>
</div>

<!-- News-->
<div id="sec-news" class="layout-outer">
  <div class="layout-inner">
    <div class="news-wrap">
      <img class="news-img" src="./imgs/news-img.png"/>
      <div class="_right">
        <h2>
          <span class="color1">A.P. Honda<br/></span>
          MOTORSPORT NEWS
        </h2>
        <a href="https://www.aphonda.co.th/honda2017/news/motorsport" target=" _blank" class="ts-btn">อ่านข่าวเพิ่มเติม</a>
      </div>
    </div>
  </div>
</div>

<!-- News-->
<div id="sec-youtube" class="layout-outer">
  <div class="layout-inner">
    <div class="yt-wrap">
      <img class="yt-img" src="./imgs/youtube-img.png"/>
      <div class="_bottom">
        <h2>
          Youtube<br/>
          ChanNel
        </h2>
        <a href="https://www.youtube.com/user/HondaMotorcycleTHA" target=" _blank" class="ts-btn ts-btn-black">ดูวิดีโอ</a>
      </div>
    </div>
  </div>
</div>

<div id="sec-bottom" class="layout-outer">
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.min.js"></script>

<!--<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>-->
<!--<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js"></script>
<script src="./js/index.js?<?php echo time(); ?>"></script>
</body>
</html>


