$(document).ready(function () {

  // Timeline
  // ----------------------------------------
  var changeSlideTimeHere = 5000 // Change slide time here
  var $timeline = $('#timeline')
  var $events = $timeline.find('.event')
  var $bike = $('#bike')
  var $cloud = $('#cloud')
  var $mobileEvents = $('#mobile-events')
  var slideInterval = null

  // Should start `auto slide` after scroll (not on load)
  // ----------------------------------------
  $(window).scroll(function () {
    if(slideInterval == null && $(window).scrollTop() > 100) {
      slideInterval = setInterval(nextSlide, changeSlideTimeHere)
    }
  });

  $timeline.find('.dot').click(function () {
    var me = $(this)
    $events.removeClass('active')
    me.parent().addClass('active').trigger('cssClassChanged')
    resetInterval()
  })
  $timeline.find('.chevron').click(function () {
    nextSlide($(this).hasClass('__left'))
  })
  $events.bind('cssClassChanged', {}, onClassChange);

  function onClassChange () {
    // mobile
    if ($(window).width() >= 768) {
      $bike.css({
        left: ($timeline.find('.event.active').offset().left - 70) + 'px',
      })
      $cloud.css({
        left: ($timeline.find('.event.active').offset().left - 1350) + 'px',
      })
    }
    // desktop
    else {
      var index = $events.index($timeline.find('.event.active'))
      $mobileEvents.find('.event-desc').removeClass('active').eq(index).addClass('active')
    }
  }

  function nextSlide (reverse = false) {
    var index = $events.index($timeline.find('.event.active'))
    var n = $events.length
    if (reverse) index -= 1
    else index += 1
    index = ((index % n) + n) % n // infinite loop
    $events.removeClass('active')
    $events.eq(index).addClass('active').trigger('cssClassChanged')
    resetInterval()
  }

  function resetInterval () {
    if(slideInterval !== null) {
      clearInterval(slideInterval)
      slideInterval = setInterval(nextSlide, changeSlideTimeHere)
    }
  }

  // Mobile Timeline
  // ----------------------------------------
  $mobileEvents.swipe({
    swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
      nextSlide(direction === 'left')
    },
  })

  // change the container
  if ($(window).width() < 768) { // sm
    $mobileEvents.append($timeline.find('.event-desc'))
  }

  // initialize things
  // ----------------------------------------
  setTimeout(function () {
    $timeline.find('.dot').first().click()
  }, 50)

});
